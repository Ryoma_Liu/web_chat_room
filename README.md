# README #

A web chat room

### What is this repository for? ###

* Chatbot development
* video conference

### How do I get set up? ###

1.Install virtualenv

```bash
pip install virtualenv
```

2.Create a new environment and a new folder venv in your root dir

```bash
virtualenv venv
```

3.Activate the environment(for windows)

```bash
venv\Scripts\activate
```

4.Install the required python packages

```bash
pip install -r requirments.txt
```

5.Run the programm

```bash
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

### Need Feature

- A side view of transcript and chat area

### Contribution guidelines ###

* 

* 