from django.urls import path

from . import views

app_name = 'listenbot'
urlpatterns = [
    path('', views.index, name='index'),
    path('meeting/', views.meeting, name = 'meeting')
]