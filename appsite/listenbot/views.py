from django.shortcuts import render

from .models import Question


def index(request):
    return render(request, 'listenbot/index.html')

def meeting(request):
    return render(request, 'listenbot/meeting.html')