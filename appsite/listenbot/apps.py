from django.apps import AppConfig


class ListenbotConfig(AppConfig):
    name = 'listenbot'
